#!/usr/bin/python
from flask import Flask, jsonify, abort, request, make_response, url_for
from flask_httpauth import HTTPBasicAuth

app = Flask(__name__, static_url_path = "")
auth = HTTPBasicAuth()

# region Error Handler
@auth.get_password
def get_password(username):
    if username == 'miguel':
        return 'python'
    return None

@auth.error_handler
def unauthorized():
    return make_response(jsonify( { 'error': 'Unauthorized access' } ), 403)
    # return 403 instead of 401 to prevent browsers from displaying the default auth dialog
    
@app.errorhandler(400)
def not_found(error):
    return make_response(jsonify( { 'error': 'Bad request' } ), 400)

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify( { 'error': 'Not found' } ), 404)
# endregion
    
@app.route('/todo/api/v1.0/tasks', methods = ['GET'])
@auth.login_required
def get_status():
    pass
    #return jsonify( { 'isDoorOpen':  } )

@app.route('/todo/api/v1.0/tasks', methods = ['POST'])
@auth.login_required
def set_door_open():
    pass
    #return jsonify( { 'isDoorOpen': } )
    
if __name__ == '__main__':
    app.run(debug = True)
